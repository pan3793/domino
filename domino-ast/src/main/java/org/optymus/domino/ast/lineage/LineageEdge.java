package org.optymus.domino.ast.lineage;

import org.jetbrains.annotations.NotNull;

/**
 * @author outma
 */
public class LineageEdge<T extends LineageContext> implements DAGEdge<T> {
    private DAGVertex<T> from, to;

    public LineageEdge(@NotNull DAGVertex<T> from, @NotNull DAGVertex<T> to) {
        this.from = from;
        this.to = to;
    }

    @Override
    public DAGVertex<T> from() {
        return this.from;
    }

    @Override
    public DAGVertex<T> to() {
        return this.to;
    }
}
