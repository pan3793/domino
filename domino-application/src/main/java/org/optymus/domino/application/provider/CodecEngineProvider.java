package org.optymus.domino.application.provider;


import org.apache.shardingsphere.shardingproxy.transport.mysql.codec.MySQLPacketCodecEngine;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CodecEngineProvider {

    @Bean
    public MySQLPacketCodecEngine packetCodecEngine() {
        return new MySQLPacketCodecEngine();
    }
}
