select distinct t1.name,
                plan_id,
                'literal' as addition_col
from dp.customer t1
         join
     (
         select c1.id                     cid,
                plan_id,
                rp.actual_amount + 100 as pay_amount,
                case
                    when rp.status = 0
                        then '零'
                    else
                        rp.status
                    end                   rp_state
         from dp.customer c1
                  join dp.contract c2 on c1.id = c2.customer_id
                  left join dp.biz_plan as rp
                            on c2.id = rp.contract_id
                                and rp.create_time > '2018-02'
     ) t2
     on t1.id = t2.cid
limit 3;