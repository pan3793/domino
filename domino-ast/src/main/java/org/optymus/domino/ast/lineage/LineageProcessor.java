package org.optymus.domino.ast.lineage;

/**
 * @author outma
 */
public interface LineageProcessor<T extends LineageContext> {
    /**
     * Handle vertex
     *
     * @param v LineageVertex
     * @see DAGVertex
     */
    void process(DAGVertex<T> v);
}
