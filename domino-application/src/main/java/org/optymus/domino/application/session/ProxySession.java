package org.optymus.domino.application.session;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.Nullable;

import java.util.List;


@Data
@RequiredArgsConstructor
public class ProxySession {

    private Channel frontendChannel;

    private Channel backendChannel;

    private volatile boolean authorized = false;

    @Nullable
    private String currentSchema;

    private int currentSequenceId = 0;

    // TODO
    // 参考 org.apache.shardingsphere.shardingproxy.backend.response.query.QueryHeader
    private List<String> currentQueryHeaders;

    // TODO
    // Select SQL AST, maybe more detailed
}
