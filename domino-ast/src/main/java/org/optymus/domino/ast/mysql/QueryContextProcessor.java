package org.optymus.domino.ast.mysql;

import org.optymus.domino.ast.antlr.MySqlParser;
import org.optymus.domino.ast.antlr.MySqlParserBaseVisitor;
import org.optymus.domino.ast.lineage.LineageProcessor;
import org.optymus.domino.ast.lineage.DAGVertex;
import org.optymus.domino.ast.lineage.NamingObject;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author outma
 */
public class QueryContextProcessor implements LineageProcessor<QueryContext> {
    public void process(DAGVertex<QueryContext> v) {
        QueryContext currentQueryContext = v.value();
        MySqlParser.QuerySpecificationContext context = currentQueryContext.value();

        List<NamingObject> selectElements = this.extractSelectElements(context.selectElements());
        List<NamingObject> tables = this.extractTables(context.fromClause());

        tidy(selectElements, tables);

        currentQueryContext.setTables(tables);
        currentQueryContext.setSelectElements(selectElements);
    }

    private void tidy(List<NamingObject> currentSelectElements, List<NamingObject> currentTables) {
        for (NamingObject selectElement : currentSelectElements) {
            int starIndex = selectElement.getAlias().indexOf("*");
            if (starIndex != -1) {
                List<NamingObject> expectedTables;
                if (starIndex == 0) {
                    expectedTables = currentTables;
                } else {
                    String realAlias = selectElement.getAlias().substring(0, starIndex - 1);
                    expectedTables = currentTables.stream().
                            filter(t -> t.getAlias().startsWith(realAlias)).
                            collect(Collectors.toList());
                }

                assignLineage4Star(selectElement, starIndex, expectedTables);
            } else {
                for (Lineage lineage : selectElement.getLineages()) {
                    String tableFull = null;
                    if (lineage.getOrigin().contains(".")) {
                        List<NamingObject> matched = currentTables.stream()
                                .filter(i -> lineage.getOrigin().startsWith(i.getAlias()))
                                .collect(Collectors.toList());
                        NamingObject bestMatch;
                        if (matched.size() > 1) {
                            // if matched multiply, pick the one has longest prefix
                            bestMatch = matched.stream().max(Comparator.comparingInt(n -> n.getAlias().length())).orElse(null);
                        } else {
                            bestMatch = matched.get(0);
                        }
                        if (bestMatch != null) {
                            tableFull = bestMatch.getLineages().get(0).getAncestor();
                        }
                    } else {
                        for (NamingObject table : currentTables) {
                            for (Lineage tableLineage : table.getLineages()) {
                                ArrayList<String> columns = DDLFetcher.getColumnsByTableFullName(
                                        tableLineage.getAncestor()
                                );
                                if (!columns.isEmpty()) {
                                    if (columns.contains(lineage.getOrigin())) {
                                        tableFull = tableLineage.getAncestor();
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    if (tableFull != null) {
                        int index = lineage.getOrigin().lastIndexOf(".");
                        if (index != -1) {
                            lineage.setAncestor(tableFull + "." + lineage.getOrigin().substring(index + 1));
                        } else {
                            lineage.setAncestor(tableFull + "." + lineage.getOrigin());
                        }
                    }
                }
            }
        }

    }

    private List<NamingObject> extractSelectElements(MySqlParser.SelectElementsContext context) {
        List<NamingObject> selectElements = new ArrayList<>();

        context.accept(new MySqlParserBaseVisitor() {
            Stack<NamingObject> sharedSelectElementStack = new Stack<>();

            @Override
            public Object visitSelectElements(MySqlParser.SelectElementsContext ctx) {
                // if select elements contains a pure "*", there is no way to trigger exitSelectStarElement()
                // so we need a special process to handle it
                if (ctx.star != null) {
                    selectElements.add(new NamingObject(
                            "*",
                            "*",
                            ctx.star.getStartIndex(),
                            ctx.star.getStopIndex()
                    ));
                }
                return super.visitSelectElements(ctx);
            }

            @Override
            public Object visitSelectFunctionElement(MySqlParser.SelectFunctionElementContext ctx) {
                assignElement(ctx);

                return super.visitSelectFunctionElement(ctx);
            }

            @Override
            public Object visitSelectColumnElement(MySqlParser.SelectColumnElementContext ctx) {
                assignElement(ctx);

                return super.visitSelectColumnElement(ctx);
            }

            @Override
            public Object visitSelectStarElement(MySqlParser.SelectStarElementContext ctx) {
                // TODO optimize: could directly get alias
                selectElements.add(new NamingObject(
                        ctx.getText(),
                        ctx.getText(),
                        ctx.start.getStartIndex(),
                        ctx.stop.getStopIndex()
                ));

                return super.visitSelectStarElement(ctx);
            }

            private <E extends MySqlParser.SelectElementContext> void assignElement(E ctx) {
                sharedSelectElementStack.push(new NamingObject());

                MySqlParser.UidContext uid = ctx.getRuleContext(MySqlParser.UidContext.class, 0);
                ctx.accept(new MySqlParserBaseVisitor() {
                    @Override
                    public Object visitFullColumnName(MySqlParser.FullColumnNameContext ctx) {
                        NamingObject namingObject = sharedSelectElementStack.peek();
                        // this means this is a literal element so we dont need to collect
                        if (ctx.getText().startsWith("'") && ctx.getText().endsWith("'")) {
                            return super.visitFullColumnName(ctx);
                        } else {
                            namingObject.getLineages().add(
                                    new Lineage(
                                            ctx.getText(),
                                            null,
                                            ctx.start.getStartIndex(),
                                            ctx.stop.getStopIndex()
                                    )
                            );
                        }
                        return super.visitFullColumnName(ctx);
                    }
                });

                NamingObject namingObject = sharedSelectElementStack.pop();
                // if an element with empty lineage means this element consists of constants(or literal values).
                // so it's unnecessary to offer to processing queue.
                if (!namingObject.getLineages().isEmpty()) {
                    namingObject.setAlias(uid == null ? ctx.getText() : uid.getText());
                    namingObject.setExpression(ctx.getText());
                    namingObject.setStartIndex(ctx.start.getStartIndex());
                    namingObject.setEndIndex(ctx.stop.getStopIndex());
                    selectElements.add(namingObject);
                }
            }
        });
        return selectElements;
    }

    private List<NamingObject> extractTables(MySqlParser.FromClauseContext context) {
        List<NamingObject> tables = new ArrayList<>();

        // only scan following from clause contexts
        context.accept(new MySqlParserBaseVisitor() {
            Stack<NamingObject> sharedTableStack = new Stack<>();

            @Override
            public Object visitAtomTableItem(MySqlParser.AtomTableItemContext ctx) {
                String alias = ctx.uid() == null ? ctx.getText() : ctx.uid().getText();
                NamingObject namingObject = new NamingObject();
                namingObject.setExpression(ctx.getText());
                namingObject.setAlias(alias);
                namingObject.getLineages().add(
                        new Lineage(
                                alias,
                                null,
                                ctx.start.getStartIndex(),
                                ctx.stop.getStopIndex()
                        )
                );

                sharedTableStack.push(namingObject);

                ctx.accept(new MySqlParserBaseVisitor() {
                    @Override
                    public Object visitTableName(MySqlParser.TableNameContext ctx) {
                        String fullName = ctx.fullId() == null ? "" : ctx.fullId().getText();
                        NamingObject namingObject = sharedTableStack.peek();
                        namingObject.getLineages().get(0).setAncestor(fullName);

                        return super.visitTableName(ctx);
                    }
                });
                tables.add(sharedTableStack.pop());

                return null;
            }

            @Override
            public Object visitSubqueryTableItem(MySqlParser.SubqueryTableItemContext ctx) {
                sharedTableStack.push(new NamingObject());

                ctx.accept(new MySqlParserBaseVisitor<Object>() {
                    @Override
                    public Object visitSubqueryTableItem(MySqlParser.SubqueryTableItemContext ctx) {
                        NamingObject namingObject = sharedTableStack.peek();
                        namingObject.setExpression(ctx.getText());
                        namingObject.setAlias(ctx.uid() == null ? "" : ctx.uid().getText());

                        return super.visitSubqueryTableItem(ctx);
                    }

                    @Override
                    public Object visitTableName(MySqlParser.TableNameContext ctx) {
                        String fullName = ctx.fullId() == null ? "" : ctx.fullId().getText();
                        NamingObject namingObject = sharedTableStack.peek();
                        namingObject.getLineages().add(new Lineage(
                                fullName,
                                fullName,
                                ctx.start.getStartIndex(),
                                ctx.stop.getStopIndex()
                        ));

                        return super.visitTableName(ctx);
                    }
                });

                tables.add(sharedTableStack.pop());

                return null;
            }
        });

        return tables;
    }

    private void assignLineage4Star(NamingObject selectElement, int starIndex, List<NamingObject> tables) {
        selectElement.getLineages().clear();
        for (NamingObject table : tables) {
            for (Lineage lineage : table.getLineages()) {
                String tableName = lineage.getAncestor();
                String alias = table.getAlias().equals(tableName) ? tableName : table.getAlias();

                ArrayList<String> columns = DDLFetcher.getColumnsByTableFullName(tableName);
                if (!columns.isEmpty()) {
                    if (starIndex == 0) {
                        for (String column : columns) {
                            selectElement.getLineages().add(new Lineage(alias + "." + column, tableName + "." + column));
                        }
                    } else {
                        String prefix = selectElement.getAlias().substring(0, starIndex - 1);
                        for (String column : columns) {
                            selectElement.getLineages().add(
                                    new Lineage(prefix + "." + column, tableName + "." + column)
                            );
                        }
                    }
                }
            }
        }
    }
}
