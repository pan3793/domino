package org.optymus.domino.application.netty.handler;

import org.optymus.domino.application.session.ProxySession;
import org.optymus.domino.application.session.ProxySessionKeeper;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class FrontendChannelStateChangeHandler extends AbstractChannelStateChangeHandler {
    @Override
    public Channel getPairedChannel(Channel currentChannel) {
        ProxySession session = ProxySessionKeeper.INSTANCE.getSessionByFrontChannelId(currentChannel.id());
        if (session == null) {
            return null;
        }
        return session.getBackendChannel();
    }

    @Override
    public void channelWritabilityChanged(ChannelHandlerContext ctx) {
        log.info("frontend channel {} writability changed", ctx.channel().id().asShortText());
        super.channelWritabilityChanged(ctx);
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) {
        log.info("frontend channel {} inactive", ctx.channel().id().asShortText());
        super.channelInactive(ctx);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        log.error("frontend channel raise exception: {}", cause.getMessage(), cause);
        super.channelInactive(ctx);
    }
}
