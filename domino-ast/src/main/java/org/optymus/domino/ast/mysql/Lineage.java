package org.optymus.domino.ast.mysql;

import org.optymus.domino.ast.antlr.SqlSegment;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * @author outma
 */
@Data
@AllArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class Lineage extends SqlSegment {
    private String origin;
    private String ancestor;

    Lineage(String origin, String ancestor, Integer startIndex, Integer stopIndex) {
        this.origin = origin;
        this.ancestor = ancestor;
        this.setStartIndex(startIndex);
        this.setEndIndex(stopIndex);
    }
}
