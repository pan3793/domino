package org.optymus.domino.application.netty;

import org.optymus.domino.application.netty.decoder.MySQLBackendPacketDecoder;
import org.optymus.domino.application.netty.decoder.MySQLFrameDecoder;
import org.optymus.domino.application.netty.handler.BackendChannelStateChangeHandler;
import org.optymus.domino.application.netty.handler.MySQLBackendPacketInboundHandler;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.util.concurrent.Future;
import lombok.Getter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

@Slf4j
@Component
public class MySQLBackendProxy {

    /**
     * 后端代理 IO 线程组
     */
    @Resource
    private EventLoopGroup backendWorkerGroup;

    @Getter
    private Bootstrap bootstrap;

    @PostConstruct
    public void init() {
        this.bootstrap = new Bootstrap()
                .group(backendWorkerGroup)
                .channel(NioSocketChannel.class)
                .handler(new ChannelInitializer<Channel>() {
                    @Override
                    protected void initChannel(Channel ch) {
                        ch.pipeline().addLast(new LoggingHandler("BackendLoggingHandler", LogLevel.INFO));
                        ch.pipeline().addLast(new MySQLFrameDecoder());
                        ch.pipeline().addLast(new BackendChannelStateChangeHandler());
                        ch.pipeline().addLast(new MySQLBackendPacketDecoder());
                        ch.pipeline().addLast(new MySQLBackendPacketInboundHandler());
                    }
                });
    }

    @PreDestroy
    @SneakyThrows
    public void destroy() {
        log.info("Shutting down backend worker group");
        Future<?> shutdownFuture = backendWorkerGroup.shutdownGracefully(10, 30, TimeUnit.SECONDS);
        shutdownFuture.addListener(future -> {
            if (future.isSuccess()) {
                log.info("Backend worker group has shutdown gracefully.");
            } else {
                log.error("Backend worker group has shutdown error: {}", future.cause().getMessage(), future.cause());
            }
        });
        shutdownFuture.sync();
    }
}
