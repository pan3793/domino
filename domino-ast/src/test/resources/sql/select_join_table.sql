select b1.id,
       b1.number,
       b1.customer_id,
       b2.amount,
       plan_id
from biz_contract as b1
         join biz_plan as b2 on b1.contract_id = b2.contract_id