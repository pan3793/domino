package org.optymus.domino.application.netty;

import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;

public interface Passthroughable extends ProxyContext {

    default void passthrough(Channel thisChannel, ByteBuf byteBuf) {
        getPairedChannel(thisChannel).writeAndFlush(byteBuf);
    }
}
