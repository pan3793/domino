select c.id,
       number as no,
       amount    price
from biz_contract as c
where amount > 100
order by id
limit 2;