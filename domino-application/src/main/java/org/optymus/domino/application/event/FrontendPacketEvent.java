package org.optymus.domino.application.event;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class FrontendPacketEvent {

    private final ChannelHandlerContext frontendChannelCtx;

    private final ByteBuf message;
}
