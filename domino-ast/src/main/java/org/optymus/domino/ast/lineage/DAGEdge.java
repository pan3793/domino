package org.optymus.domino.ast.lineage;

/**
 * @author outma
 */
public interface DAGEdge<T> {
    /**
     * @return vertex from
     * @see DAGVertex
     */
    DAGVertex<T> from();

    /**
     * @return vertex to
     * @see DAGVertex
     */
    DAGVertex<T> to();
}
