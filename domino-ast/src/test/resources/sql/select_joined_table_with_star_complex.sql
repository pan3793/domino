select dp2.*, plan_id, 'literal' as 'dummy'
from (
         select *
         from dp.contract
     ) dp1
         join (
    select *
    from dp.contract_extra
) dp2 on dp1.id = dp2.id
         join dp.biz_plan dp3 on dp3.plan_id = dp2.id