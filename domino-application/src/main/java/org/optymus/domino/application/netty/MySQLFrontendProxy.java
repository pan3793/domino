package org.optymus.domino.application.netty;

import org.optymus.domino.application.netty.decoder.MySQLFrameDecoder;
import org.optymus.domino.application.netty.decoder.MySQLFrontendPacketDecoder;
import org.optymus.domino.application.netty.handler.FrontendChannelStateChangeHandler;
import org.optymus.domino.application.netty.handler.MySQLFrontendHandshakeHandler;
import org.optymus.domino.application.netty.handler.MySQLFrontendPacketInboundHandler;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.util.concurrent.Future;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;

@Slf4j
@Component
@RequiredArgsConstructor
public class MySQLFrontendProxy {

    /**
     * 前端代理 reactor 线程组
     */
    @Resource
    private EventLoopGroup frontendBossGroup;

    /**
     * 前端代理 IO 线程组
     */
    @Resource
    private EventLoopGroup frontendWorkerGroup;

    @Getter
    private ServerBootstrap serverBootstrap;

    @PostConstruct
    public void init() {
        this.serverBootstrap = new ServerBootstrap()
                .group(frontendBossGroup, frontendWorkerGroup)
                .channel(NioServerSocketChannel.class)
                .handler(new LoggingHandler("FrontendLoggingHandler", LogLevel.INFO))
                .childHandler(new ChannelInitializer<SocketChannel>() {
                    @Override
                    protected void initChannel(SocketChannel ch) {
                        ch.pipeline().addLast(new LoggingHandler("FrontendLoggingHandler", LogLevel.INFO));
                        ch.pipeline().addLast(new MySQLFrameDecoder());
                        ch.pipeline().addLast(new FrontendChannelStateChangeHandler());
                        ch.pipeline().addLast(new MySQLFrontendHandshakeHandler());
                        ch.pipeline().addLast(new MySQLFrontendPacketDecoder());
                        ch.pipeline().addLast(new MySQLFrontendPacketInboundHandler());
                    }
                });
    }

    @PreDestroy
    @SneakyThrows
    public void destroy() {
        log.info("Shutting down frontend boss group");
        log.info("Shutting down frontend worker group");
        Future<?> bossGroupShutdownFuture = frontendBossGroup.shutdownGracefully();
        bossGroupShutdownFuture.addListener(future -> {
            if (future.isSuccess()) {
                log.info("Frontend boss group has shutdown gracefully.");
            } else {
                log.error("Frontend boss group has shutdown error: {}", future.cause().getMessage(), future.cause());
            }
        });
        Future<?> workerGroupShutdownFuture = frontendWorkerGroup.shutdownGracefully();
        workerGroupShutdownFuture.addListener(future -> {
            if (future.isSuccess()) {
                log.info("Frontend worker group has shutdown gracefully.");
            } else {
                log.error("Frontend worker group has shutdown error: {}", future.cause().getMessage(), future.cause());
            }
        });
    }
}
