package org.optymus.domino.application.netty.encoder;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import org.apache.shardingsphere.shardingproxy.transport.mysql.packet.MySQLPacket;
import org.apache.shardingsphere.shardingproxy.transport.mysql.payload.MySQLPacketPayload;

public class MySQLPacketEncoder extends MessageToByteEncoder<MySQLPacket> {

    @Override
    protected void encode(ChannelHandlerContext ctx, MySQLPacket packet, ByteBuf out) {
        try (MySQLPacketPayload payload = new MySQLPacketPayload(ctx.alloc().buffer())) {
            packet.write(payload);
            out.writeMediumLE(payload.getByteBuf().readableBytes());
            out.writeByte(packet.getSequenceId());
            out.writeBytes(payload.getByteBuf());
        }
    }
}
