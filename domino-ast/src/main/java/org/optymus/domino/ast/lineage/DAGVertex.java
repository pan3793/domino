package org.optymus.domino.ast.lineage;

import org.jetbrains.annotations.NotNull;

import java.util.Set;

/**
 * @author outma
 */
public interface DAGVertex<T> {
    /**
     * Add a child to current vertex
     *
     * @param v {@link DAGVertex}
     */
    void addChild(DAGVertex<T> v);

    /**
     * Get children of current vertex
     *
     * @return Set<Vertex>
     * @see DAGVertex
     */
    Set<DAGVertex<T>> children();

    /**
     * Get value of current vertex
     *
     * @return T
     */
    @NotNull
    T value();
}
