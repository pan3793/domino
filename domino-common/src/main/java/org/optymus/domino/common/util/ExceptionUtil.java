package org.optymus.domino.common.util;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ExceptionUtil {

    public interface ExceptionBlock {
        void run() throws Throwable;
    }

    public interface ExceptionSupplier<R> {
        R get() throws Throwable;
    }

    public static void convertToRuntimeEx(ExceptionBlock block) {
        try {
            block.run();
        } catch (Throwable ex) {
            throw new RuntimeException(ex);
        }
    }

    public static <R> R convertToRuntimeEx(ExceptionSupplier<R> supplier) {
        try {
            return supplier.get();
        } catch (Throwable ex) {
            throw new RuntimeException(ex);
        }
    }
}
