package org.optymus.domino.application.event;

import io.netty.channel.ChannelHandlerContext;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class FrontendHandshakeEvent {

    private final ChannelHandlerContext frontendChannelCtx;

}
