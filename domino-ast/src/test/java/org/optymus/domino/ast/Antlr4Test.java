package org.optymus.domino.ast;

import org.optymus.domino.ast.antlr.CaseChangingCharStream;
import org.optymus.domino.ast.antlr.MySqlLexer;
import org.optymus.domino.ast.antlr.MySqlParser;
import org.optymus.domino.ast.antlr.MySqlParserBaseListener;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.antlr.v4.runtime.*;
import org.junit.Assert;
import org.junit.Test;

import java.io.InputStream;


@Slf4j
public class Antlr4Test {

    static class ParserListener extends MySqlParserBaseListener {

        @Override
        public void enterSelectElements(MySqlParser.SelectElementsContext ctx) {
            log.info("enter select elements {}", ctx.getText());
        }

        @Override
        public void exitSelectElements(MySqlParser.SelectElementsContext ctx) {
            log.info("exit select elements {}", ctx.getText());
        }
    }

    static class ParseErrorListener extends BaseErrorListener {

        @Override
        public void syntaxError(Recognizer<?, ?> recognizer,
                                Object offendingSymbol,
                                int line,
                                int charPositionInLine,
                                String msg,
                                RecognitionException e) {
            log.error(msg);
            throw new RuntimeException(msg);
        }
    }

    @Test
    @SneakyThrows
    public void testAntlr4() {
        String sqlFileName = "sql/bitrix_queries_cut.sql";
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(sqlFileName);
        Assert.assertNotNull("sql file " + sqlFileName + " not exist!", inputStream);

        CharStream charStream = CharStreams.fromStream(inputStream);
        MySqlLexer lexer = new MySqlLexer(new CaseChangingCharStream(charStream, true));
        MySqlParser parser = new MySqlParser(new CommonTokenStream(lexer));

        parser.removeErrorListeners();
        parser.addErrorListener(new ParseErrorListener());
        parser.addParseListener(new ParserListener());

        parser.root();
    }
}
