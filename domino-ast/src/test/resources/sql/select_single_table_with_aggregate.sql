select sum(distinct amount) all_amount,
       'literal' as         addition_col
from dp.contract
group by customer_id
having all_amount > 1000;