package org.optymus.domino.ast.lineage;

import java.util.List;

/**
 * @author outma
 */
public interface LineageContext<T> {
    /**
     * Get value of context
     *
     * @return T
     */
    T value();

    /**
     * Get elements in select statement
     *
     * @return elements in select statement
     * @see NamingObject
     */
    List<NamingObject> selectElements();

    /**
     * Get tables in select statement
     *
     * @return tables in select statement
     * @see NamingObject
     */
    List<NamingObject> tables();
}
