package org.optymus.domino.ast.mysql;

import org.optymus.domino.ast.antlr.MySqlParser;
import org.optymus.domino.ast.antlr.MySqlParserBaseListener;
import org.optymus.domino.ast.antlr.MySqlParserBaseVisitor;
import org.optymus.domino.ast.lineage.LineageDAG;
import org.optymus.domino.ast.lineage.LineageEdge;
import org.optymus.domino.ast.lineage.LineageVertex;
import lombok.extern.slf4j.Slf4j;

/**
 * @author outma
 */
@Slf4j
public class QueryParserListener extends MySqlParserBaseListener {
    private LineageDAG<QueryContext> lineageDAG = new LineageDAG<>();

    @Override
    public void enterQuerySpecification(MySqlParser.QuerySpecificationContext currentCtx) {
        log.debug("enter query spec");

        currentCtx.accept(new MySqlParserBaseVisitor() {
            @Override
            public Object visitQuerySpecification(MySqlParser.QuerySpecificationContext ctx) {
                lineageDAG.addEdge(new LineageEdge<>(
                        new LineageVertex<>(new QueryContext(currentCtx)),
                        new LineageVertex<>(new QueryContext(ctx)))
                );

                return null;
            }
        });
    }

    public LineageDAG<QueryContext> getLineageDAG() {
        return this.lineageDAG;
    }
}
