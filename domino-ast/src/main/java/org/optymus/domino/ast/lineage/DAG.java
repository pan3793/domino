package org.optymus.domino.ast.lineage;

import java.util.Set;

/**
 * @author outma
 */
public interface DAG<T> {
    /**
     * Return the vertex count of this graph
     *
     * @return vertex count of this graph
     */
    int size();

    /**
     * Add an edge to this graph
     *
     * @param e edge
     * @see DAGEdge
     */
    void addEdge(DAGEdge<T> e);

    /**
     * Return all vertices
     *
     * @return all vertices
     * @see DAGVertex
     */
    Set<DAGVertex<T>> vertices();

    /**
     * Return the root vertex of this graph
     *
     * @return root vertex
     * @see DAGVertex
     */
    DAGVertex<T> root();
}
