package org.optymus.domino.ast.catalog;

import lombok.Data;

@Data
public class Database {
    private String name;
    private String engine;
    private String comment;
}
