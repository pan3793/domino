package org.optymus.domino.ast.catalog;

import java.util.List;

public interface Catalog {

    List<String> listDatabases();

    boolean databaseExist(String database);

    void setCurrentDatabase(String database);

    String getCurrentDatabase();

    default List<String> listTables() {
        return listTables(getCurrentDatabase());
    }

    List<String> listTables(String database);

    default boolean tableExist(String table) {
        return tableExist(getCurrentDatabase(), table);
    }

    boolean tableExist(String database, String table);

    Table getTable(String database, String table);
}
