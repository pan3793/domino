package org.optymus.domino.application.netty.handler;

import org.optymus.domino.application.netty.ProxyContext;
import org.optymus.domino.application.session.ProxySessionKeeper;
import io.netty.channel.Channel;
import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;

/**
 * FIXME: need detailed achievement when we can resolve MYSQL packet
 */
@Slf4j
public abstract class AbstractChannelStateChangeHandler extends ChannelDuplexHandler implements ProxyContext {

    protected void deregister(Channel currentChannel) {
        ArrayList<Channel> releasableChannels = new ArrayList<>(2);
        releasableChannels.add(currentChannel);

        Channel pairedChannel = getPairedChannel(currentChannel);
        if (pairedChannel != null) {
            releasableChannels.add(pairedChannel);
        }

        releasableChannels.forEach(channel -> {
            // deregister session
            ProxySessionKeeper.INSTANCE.deregister(currentChannel);
            // deregister channel
            channel.close().addListener((f) -> {
                if (f.isSuccess()) {
                    log.info("channel {} closed", channel.id().asShortText());
                } else {
                    log.error("fail to close channel {}", channel.id().asShortText(), f.cause());
                }
            });
        });
    }

    @Override
    public void channelWritabilityChanged(ChannelHandlerContext ctx) {
        if (getPairedChannel(ctx.channel()) != null && getPairedChannel(ctx.channel()).isWritable()) {
            deregister(ctx.channel());
        }
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) {
        if (getPairedChannel(ctx.channel()) != null && getPairedChannel(ctx.channel()).isActive()) {
            deregister(ctx.channel());
        }
    }
}
