package org.optymus.domino.application.netty.handler;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.apache.shardingsphere.shardingproxy.transport.mysql.packet.MySQLPacket;

public class MySQLFrontendPacketInboundHandler extends SimpleChannelInboundHandler<MySQLPacket> {

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, MySQLPacket msg) {

    }
}