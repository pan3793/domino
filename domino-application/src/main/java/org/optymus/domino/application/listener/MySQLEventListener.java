package org.optymus.domino.application.listener;

import org.optymus.domino.application.conf.ProxyTable;
import org.optymus.domino.application.session.ProxySession;
import org.optymus.domino.application.event.BackendPacketEvent;
import org.optymus.domino.application.event.FrontendHandshakeEvent;
import org.optymus.domino.application.event.FrontendPacketEvent;
import org.optymus.domino.application.netty.MySQLBackendProxy;
import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelId;
import lombok.Getter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.apache.shardingsphere.shardingproxy.error.CommonErrorCode;
import org.apache.shardingsphere.shardingproxy.transport.mysql.codec.MySQLPacketCodecEngine;
import org.apache.shardingsphere.shardingproxy.transport.mysql.packet.generic.MySQLErrPacket;
import org.apache.shardingsphere.shardingproxy.transport.mysql.payload.MySQLPacketPayload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.net.InetSocketAddress;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * 该类现有实现已被废弃, 现有逻辑被新的pipeline所取代.
 * 但spring event机制可继续使用, 以便于报文开发过程中旁路测试
 * 即: 不干预passthrough机制情况下, 拷贝ByteBuf进行Packet解析, 要特别注意ByteBuf引用计数机制
 */
@Slf4j
@Component
@Deprecated
public class MySQLEventListener {

    /**
     * Backend Proxy client bootstrap
     */
    private Bootstrap bootstrap;

    @Resource
    private MySQLPacketCodecEngine packetCodecEngine;

    @Resource
    private ProxyTable proxyTable;

    // TODO
    // 抽象Register
    // 连接断开时反注册
    @Getter
    private Map<ChannelId, ProxySession> frontendProxyMapping = new ConcurrentHashMap<>();
    @Getter
    private Map<ChannelId, ProxySession> backendProxyMapping = new ConcurrentHashMap<>();

    @Autowired
    public MySQLEventListener(MySQLBackendProxy backendProxy) {
        this.bootstrap = backendProxy.getBootstrap();
    }

    @EventListener
    public void onFrontendHandshakeEvent(FrontendHandshakeEvent event) {
        log.info("Recv FrontendHandshakeEvent");
        Channel frontendChannel = event.getFrontendChannelCtx().channel();
        ChannelId frontendChannelId = frontendChannel.id();
        ProxySession proxyContext = this.buildProxyContext(frontendChannel);
        this.frontendProxyMapping.put(frontendChannelId, proxyContext);
        this.backendProxyMapping.put(proxyContext.getBackendChannel().id(), proxyContext);
    }

    @EventListener
    public void onFrontendPacketEvent(FrontendPacketEvent event) {
        log.info("Recv FrontendPacketEvent");
        Channel frontendChannel = event.getFrontendChannelCtx().channel();
        ChannelId frontendChannelId = frontendChannel.id();
        Channel backendChannel = null;
        ProxySession proxyContext = null;
        try {
            proxyContext = this.frontendProxyMapping.get(frontendChannelId);
            assert proxyContext != null;
            backendChannel = proxyContext.getBackendChannel();

            try (MySQLPacketPayload frontendPayload = packetCodecEngine.createPacketPayload(event.getMessage())) {
                ByteBuf buffer = backendChannel.alloc().buffer();
                buffer.writeMediumLE(frontendPayload.getByteBuf().readableBytes() - 1);
                buffer.writeBytes(frontendPayload.getByteBuf());
                backendChannel.writeAndFlush(buffer);
            }
        } catch (Throwable cause) {
            int sequenceId = 0;
            if (proxyContext != null) {
                sequenceId = proxyContext.getCurrentSequenceId();
            }
            onError(sequenceId, frontendChannel, cause);
        }
    }

    private ProxySession buildProxyContext(Channel frontendChannel) {
        ProxySession proxyContext = new ProxySession();
        proxyContext.setFrontendChannel(frontendChannel);
        proxyContext.setBackendChannel(this.connectBackend(frontendChannel));
        return proxyContext;
    }

    @EventListener
    public void onBackPacketEvent(BackendPacketEvent event) {
        log.info("Recv BackendPacketEvent");
        Channel frontendChannel = null;
        Channel backendChannel = event.getBackendChannelCtx().channel();
        ChannelId backendChannelId = backendChannel.id();
        ProxySession proxyContext = null;
        int sequenceId = 0;
        try {
            proxyContext = this.backendProxyMapping.get(backendChannelId);
            assert proxyContext != null;
            sequenceId = proxyContext.getCurrentSequenceId();
            frontendChannel = proxyContext.getFrontendChannel();
            assert frontendChannel != null;

            try (val backendPayload = packetCodecEngine.createPacketPayload(event.getMessage())) {
                ByteBuf buffer = frontendChannel.alloc().buffer();
                buffer.writeMediumLE(backendPayload.getByteBuf().readableBytes() - 1);
                buffer.writeBytes(backendPayload.getByteBuf());
                frontendChannel.writeAndFlush(buffer);
            }
        } catch (Throwable cause) {
            onError(sequenceId, frontendChannel, cause);
        }
    }


    @SneakyThrows
    private Channel connectBackend(Channel frontendChannel) {
        InetSocketAddress localAddr = (InetSocketAddress) frontendChannel.localAddress();

        ProxyTable.Item item = proxyTable.findByLocal("0.0.0.0", localAddr.getPort())
                .orElseThrow(() -> new RuntimeException("no proxy table item"));

        ChannelFuture channelFuture = bootstrap.connect(item.getRemoteIp(), item.getRemotePort()).sync();
        if (!channelFuture.isSuccess()) {
            throw new RuntimeException("cannot connect to MySQL server", channelFuture.cause());
        }
        return channelFuture.channel();
    }

    private void onError(int sequenceId, Channel frontendChannel, Throwable cause) {
        // TODO 精细化处理异常
        log.error(cause.getMessage(), cause);
        frontendChannel.writeAndFlush(
                new MySQLErrPacket(sequenceId, CommonErrorCode.UNKNOWN_EXCEPTION, cause.getMessage())
        );
    }
}
