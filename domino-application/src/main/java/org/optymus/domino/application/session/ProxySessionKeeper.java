package org.optymus.domino.application.session;

import io.netty.channel.Channel;
import io.netty.channel.ChannelId;
import lombok.SneakyThrows;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author outma
 */
public enum ProxySessionKeeper {
    /**
     * 单例
     */
    INSTANCE;

    private long toleranceMs = 200;
    private Map<ChannelId, ProxySession> frontendProxyMapping = new ConcurrentHashMap<>();
    private Map<ChannelId, ProxySession> backendProxyMapping = new ConcurrentHashMap<>();

    /**
     * Register a proxy session
     *
     * @param frontend frontend channel
     * @param backend  backend channel
     */
    public void register(Channel frontend, Channel backend) {
        ProxySession session = new ProxySession();
        session.setFrontendChannel(frontend);
        session.setBackendChannel(backend);

        frontendProxyMapping.put(frontend.id(), session);
        backendProxyMapping.put(backend.id(), session);
    }

    /**
     * Deregister proxy session by current channel
     *
     * @param channel current channel
     */
    public void deregister(Channel channel) {
        frontendProxyMapping.remove(channel.id());
        backendProxyMapping.remove(channel.id());
    }

    public ProxySession getSessionByFrontChannelId(ChannelId frontId) {
        return frontendProxyMapping.get(frontId);
    }

    @SneakyThrows
    public ProxySession getSessionByBackendChannelId(ChannelId backendId) {
        ProxySession session = backendProxyMapping.get(backendId);
        if (session == null) {
            Thread.sleep(toleranceMs);
            session = backendProxyMapping.get(backendId);
        }

        return session;
    }
}
