package org.optymus.domino.application.netty.handler;

import org.optymus.domino.application.session.ProxySession;
import org.optymus.domino.application.session.ProxySessionKeeper;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.slf4j.Slf4j;

/**
 * @author outma
 */
@Slf4j
public class BackendChannelStateChangeHandler extends AbstractChannelStateChangeHandler {
    @Override
    public Channel getPairedChannel(Channel currentChannel) {
        ProxySession session = ProxySessionKeeper.INSTANCE.getSessionByBackendChannelId(currentChannel.id());
        if (session == null) {
            return null;
        }
        return session.getFrontendChannel();
    }

    @Override
    public void channelWritabilityChanged(ChannelHandlerContext ctx) {
        log.info("backend channel {} writability changed", ctx.channel().id().asShortText());
        super.channelWritabilityChanged(ctx);
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) {
        log.info("backend channel {} inactive", ctx.channel().id().asShortText());
        super.channelInactive(ctx);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        log.error("backend channel raise exception: {}", cause.getMessage(), cause);
        // TODO 构造ErrorPacket
        super.channelInactive(ctx);
    }
}
