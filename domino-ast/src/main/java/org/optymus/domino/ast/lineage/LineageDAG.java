package org.optymus.domino.ast.lineage;

import java.util.HashSet;
import java.util.Set;

/**
 * @author outma
 */
public class LineageDAG<T extends LineageContext> implements DAG<T> {
    private Set<DAGVertex<T>> holder = new HashSet<>();
    private DAGVertex<T> first;

    @Override
    public int size() {
        return this.holder.size();
    }

    @Override
    public void addEdge(DAGEdge<T> e) {
        if (this.holder.isEmpty()) {
            first = e.from();
        }

        this.holder.add(e.from());
        this.holder.add(e.to());

        e.from().addChild(e.to());
    }

    @Override
    public Set<DAGVertex<T>> vertices() {
        return this.holder;
    }

    @Override
    public DAGVertex<T> root() {
        return this.first;
    }
}
