CREATE DATABASE IF NOT EXISTS `test` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;

CREATE TABLE `test`.`customer`
(
    `id`          varchar(36) NOT NULL DEFAULT '',
    `name`        varchar(50)          DEFAULT NULL COMMENT '姓名 ',
    `nick_name`   varchar(50)          DEFAULT NULL COMMENT '昵称',
    `cert_number` varchar(20)          DEFAULT NULL COMMENT '证件号',
    `phone`       varchar(20)          DEFAULT NULL COMMENT '手机',
    `create_time` datetime             DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4 COMMENT ='客户-基本信息表';

CREATE TABLE `test`.`contract`
(
    `id`                 bigint(20) NOT NULL AUTO_INCREMENT COMMENT '自增主键id',
    `number`             varchar(36)     DEFAULT NULL COMMENT '合同号',
    `customer_id`        varchar(36)     DEFAULT NULL COMMENT '客户id ',
    `apply_approve_time` timestamp  NULL DEFAULT NULL COMMENT '申请审核time',
    `amount`             decimal(18, 4)  DEFAULT '0.0000' COMMENT '金额',
    `actual_pay_time`    datetime        DEFAULT NULL COMMENT '打款时间',
    `status`             varchar(5)      DEFAULT NULL COMMENT '状态（-1 拒绝, 0未生效，1已生效，2正常结束，3提前终止，4取消）',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 5544
  DEFAULT CHARSET = utf8 COMMENT ='业务-合同表';

CREATE TABLE `test`.`repayment_plan`
(
    `id`                 varchar(36)  NOT NULL COMMENT 'pk',
    `contract_id`        bigint(20)        DEFAULT NULL COMMENT '业务id',
    `customer_id`        varchar(36)       DEFAULT NULL COMMENT '客户id',
    `principal_amount`   decimal(18, 4)    DEFAULT NULL COMMENT '本金',
    `interest_amount`    decimal(18, 4)    DEFAULT NULL COMMENT '利息',
    `total_amount`       decimal(18, 4)    DEFAULT NULL COMMENT '应还款金额 ',
    `actual_amount`      decimal(18, 4)    DEFAULT NULL COMMENT '实际已扣款金额',
    `amount_detail`      text COMMENT '金额明细, JSON',
    `amount_detail_json` json              DEFAULT NULL COMMENT '金额明细, 原生JSON',
    `overdue_days`       int(10) unsigned  DEFAULT '0' COMMENT '逾期天数',
    `debit_start_date`   datetime          DEFAULT NULL COMMENT '扣款起始日期',
    `debit_end_date`     datetime(6)       DEFAULT NULL COMMENT '扣款终止日期',
    `total_peroid`       int(11)           DEFAULT NULL COMMENT '总还款期数',
    `seq`                int(11)           DEFAULT NULL COMMENT '当前期数',
    `status`             varchar(5)        DEFAULT NULL COMMENT '扣款状态，0未扣款，1已扣款，2扣款失败，-1取消',
    `create_time`        timestamp    NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `update_time`        timestamp(3) NULL DEFAULT CURRENT_TIMESTAMP(3) COMMENT '更新时间',
    `ext_a0`             varchar(50)       DEFAULT '' COMMENT '扩展字段',
    `ext_a1`             varchar(50)       DEFAULT '1' COMMENT '扩展字段',
    `ext_a2`             varchar(50)       DEFAULT '2' COMMENT '扩展字段',
    `ext_a3`             varchar(50)       DEFAULT '3' COMMENT '扩展字段',
    `ext_a4`             varchar(50)       DEFAULT '4' COMMENT '扩展字段',
    `ext_a5`             varchar(50)       DEFAULT '5' COMMENT '扩展字段',
    `ext_a6`             varchar(50)       DEFAULT '6' COMMENT '扩展字段',
    `ext_a7`             varchar(50)       DEFAULT '7' COMMENT '扩展字段',
    `ext_a8`             varchar(50)       DEFAULT '8' COMMENT '扩展字段',
    `ext_a9`             varchar(50)       DEFAULT '9' COMMENT '扩展字段',
    `ext_aa`             varchar(50)       DEFAULT NULL COMMENT '扩展字段',
    `ext_ab`             varchar(50)       DEFAULT NULL COMMENT '扩展字段',
    `ext_ac`             varchar(50)       DEFAULT NULL COMMENT '扩展字段',
    `ext_ad`             varchar(50)       DEFAULT NULL COMMENT '扩展字段',
    `ext_ae`             varchar(50)       DEFAULT NULL COMMENT '扩展字段',
    `ext_af`             varchar(50)       DEFAULT NULL COMMENT '扩展字段',
    `ext_ag`             varchar(50)       DEFAULT NULL COMMENT '扩展字段',
    `ext_ah`             varchar(50)       DEFAULT NULL COMMENT '扩展字段',
    `ext_ai`             varchar(50)       DEFAULT NULL COMMENT '扩展字段',
    `ext_aj`             varchar(50)       DEFAULT NULL COMMENT '扩展字段',
    `ext_ak`             varchar(50)       DEFAULT NULL COMMENT '扩展字段',
    `ext_al`             varchar(50)       DEFAULT NULL COMMENT '扩展字段',
    `ext_am`             varchar(50)       DEFAULT NULL COMMENT '扩展字段',
    `ext_an`             varchar(50)       DEFAULT NULL COMMENT '扩展字段',
    `ext_ao`             varchar(50)       DEFAULT NULL COMMENT '扩展字段',
    `ext_ap`             varchar(50)       DEFAULT NULL COMMENT '扩展字段',
    `ext_aq`             varchar(50)       DEFAULT NULL COMMENT '扩展字段',
    `ext_ar`             varchar(50)       DEFAULT NULL COMMENT '扩展字段',
    `ext_as`             varchar(50)       DEFAULT NULL COMMENT '扩展字段',
    `ext_at`             varchar(50)       DEFAULT NULL COMMENT '扩展字段',
    `ext_au`             varchar(50)       DEFAULT NULL COMMENT '扩展字段',
    `ext_av`             varchar(50)       DEFAULT NULL COMMENT '扩展字段',
    `ext_aw`             varchar(50)       DEFAULT NULL COMMENT '扩展字段',
    `ext_ax`             varchar(50)       DEFAULT NULL COMMENT '扩展字段',
    `ext_ay`             varchar(50)       DEFAULT NULL COMMENT '扩展字段',
    `ext_az`             varchar(50)       DEFAULT NULL COMMENT '扩展字段',
    PRIMARY KEY (`id`),
    KEY `idx_customer_id` (`customer_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8 COMMENT ='业务-还款计划详情';