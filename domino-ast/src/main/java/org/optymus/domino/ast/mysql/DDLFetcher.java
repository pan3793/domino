package org.optymus.domino.ast.mysql;

import java.util.*;

/**
 * @author outma
 */
public class DDLFetcher {
    private static Map<String, ArrayList<String>> DDLMap = new HashMap<>();

    static {
        // TODO dynamically load like datasource routing
        DDLMap.put("biz_contract", new ArrayList<>(Arrays.asList("id", "number", "customer_id", "amount")));
        DDLMap.put("dp.contract", new ArrayList<>(Arrays.asList("id", "number", "customer_id", "amount")));
        DDLMap.put("dp.contract_extra", new ArrayList<>(Arrays.asList("id", "number", "customer_id", "extra")));
        DDLMap.put("biz_plan", new ArrayList<>(Arrays.asList("plan_id", "amount")));
        DDLMap.put("dp.biz_plan", new ArrayList<>(Arrays.asList("plan_id", "amount")));
        DDLMap.put("dp.customer", new ArrayList<>(Collections.singletonList("id")));
    }

    public static ArrayList<String> getColumnsByTableFullName(String tableFullName) {
        return DDLMap.get(tableFullName) == null ? new ArrayList<>() : DDLMap.get(tableFullName);
    }
}
