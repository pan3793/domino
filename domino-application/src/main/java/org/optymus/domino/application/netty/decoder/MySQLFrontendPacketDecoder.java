package org.optymus.domino.application.netty.decoder;

import org.optymus.domino.application.session.ProxySessionKeeper;
import io.netty.buffer.ByteBuf;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;

import java.util.List;

public class MySQLFrontendPacketDecoder extends MySQLPacketDecoder {

    @Override
    public Channel getPairedChannel(Channel currentChannel) {
        return ProxySessionKeeper.INSTANCE.getSessionByFrontChannelId(currentChannel.id()).getBackendChannel();
    }

    @Override
    protected boolean needPassthrough() {
        return true;
    }

    @Override
    protected void doDecode(ChannelHandlerContext ctx, ByteBuf frame, List<Object> out) {

    }
}
