package org.optymus.domino.ast.mysql;

import org.optymus.domino.ast.antlr.MySqlParser;
import org.optymus.domino.ast.lineage.LineageContext;
import org.optymus.domino.ast.lineage.NamingObject;

import java.util.ArrayList;
import java.util.List;

/**
 * @author outma
 */
public class QueryContext implements LineageContext<MySqlParser.QuerySpecificationContext> {
    private MySqlParser.QuerySpecificationContext context;
    private List<NamingObject> selectElements;
    private List<NamingObject> tables;

    QueryContext(MySqlParser.QuerySpecificationContext context) {
        this.context = context;
        selectElements = new ArrayList<>();
        tables = new ArrayList<>();
    }

    /**
     * Get value of context
     *
     * @return MySqlParser.QuerySpecificationContext
     * @see MySqlParser.QuerySpecificationContext
     */
    @Override
    public MySqlParser.QuerySpecificationContext value() {
        return this.context;
    }

    /**
     * Get elements in select statement
     *
     * @return elements in select statement
     * @see NamingObject
     */
    @Override
    public List<NamingObject> selectElements() {
        return selectElements;
    }

    /**
     * Get tables in select statement
     *
     * @return tables in select statement
     * @see NamingObject
     */
    @Override
    public List<NamingObject> tables() {
        return tables;
    }

    void setSelectElements(List<NamingObject> selectElements) {
        this.selectElements = selectElements;
    }

    void setTables(List<NamingObject> tables) {
        this.tables = tables;
    }
}
