package org.optymus.domino.ast.catalog;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Field {
    private Integer position;
    private String name;
    private String type;
    private Boolean nullable;
    private String defaultValue;
    private String comment;
}
