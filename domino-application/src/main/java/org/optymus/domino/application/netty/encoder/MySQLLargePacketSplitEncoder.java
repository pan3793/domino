package org.optymus.domino.application.netty.encoder;

import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageEncoder;
import org.apache.shardingsphere.shardingproxy.transport.mysql.packet.MySQLPacket;

import java.util.List;

public class MySQLLargePacketSplitEncoder extends MessageToMessageEncoder<MySQLPacket> {

    @Override
    protected void encode(ChannelHandlerContext ctx, MySQLPacket packet, List<Object> out) {
        // todo split large packet and rewrite seq_id
        out.add(packet);
    }
}
