package org.optymus.domino.ast.lineage;

import org.jetbrains.annotations.NotNull;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * @author outma
 */
public class LineageVertex<T extends LineageContext> implements DAGVertex<T> {
    private T value;
    private Set<DAGVertex<T>> children;

    public LineageVertex(@NotNull T value) {
        this.value = value;
        this.children = new LinkedHashSet<>();
    }

    @Override
    public void addChild(DAGVertex<T> v) {
        this.children.add(v);
    }

    @Override
    public Set<DAGVertex<T>> children() {
        return this.children;
    }

    @Override
    @NotNull
    public T value() {
        return this.value;
    }
}
