package org.optymus.domino.application.netty;

import io.netty.channel.Channel;

public interface ProxyContext {

    /**
     * Get paired channel by current channel
     * @param currentChannel  current channel
     * @return paired channel(may be null)
     */
    Channel getPairedChannel(Channel currentChannel);
}
