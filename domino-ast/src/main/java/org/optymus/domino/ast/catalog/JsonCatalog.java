package org.optymus.domino.ast.catalog;

import java.util.List;

public class JsonCatalog implements Catalog {

    @Override
    public List<String> listDatabases() {
        return null;
    }

    @Override
    public boolean databaseExist(String database) {
        return false;
    }

    @Override
    public void setCurrentDatabase(String database) {

    }

    @Override
    public String getCurrentDatabase() {
        return null;
    }

    @Override
    public List<String> listTables(String database) {
        return null;
    }

    @Override
    public boolean tableExist(String database, String table) {
        return false;
    }

    @Override
    public Table getTable(String database, String table) {
        return null;
    }
}
