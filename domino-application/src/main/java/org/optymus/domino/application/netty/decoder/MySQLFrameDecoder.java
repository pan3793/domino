package org.optymus.domino.application.netty.decoder;

import io.netty.handler.codec.LengthFieldBasedFrameDecoder;

import java.nio.ByteOrder;

public class MySQLFrameDecoder extends LengthFieldBasedFrameDecoder {

    public MySQLFrameDecoder() {
        super(ByteOrder.LITTLE_ENDIAN, 0xffffff, 0, 3, 1, 0, true);
    }
}
