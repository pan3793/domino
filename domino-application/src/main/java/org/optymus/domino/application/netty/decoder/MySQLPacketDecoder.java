package org.optymus.domino.application.netty.decoder;

import org.optymus.domino.application.netty.Passthroughable;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageDecoder;

import java.util.List;

/**
 * ByteToMessageDecoder 对 frame 探测有额外的处理, 会引入额外的复杂度
 * MessageToMessageDecoder<ByteBuf> 更简单
 */
public abstract class MySQLPacketDecoder extends MessageToMessageDecoder<ByteBuf> implements Passthroughable {

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf frame, List<Object> out) {
        if (needPassthrough()) {
            // MessageToMessageDecoder 默认会释放传入的消息
            // 如需 passthrough, 需手动增加引用计数
            frame.retain();
            passthrough(ctx.channel(), frame);
            return;
        }
        doDecode(ctx, frame, out);
    }

    protected abstract boolean needPassthrough();

    protected abstract void doDecode(ChannelHandlerContext ctx, ByteBuf frame, List<Object> out);
}
