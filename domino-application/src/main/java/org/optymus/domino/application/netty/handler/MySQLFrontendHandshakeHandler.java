package org.optymus.domino.application.netty.handler;

import org.optymus.domino.application.Application;
import org.optymus.domino.application.conf.ProxyTable;
import org.optymus.domino.application.netty.MySQLBackendProxy;
import org.optymus.domino.application.netty.Passthroughable;
import org.optymus.domino.application.session.ProxySessionKeeper;
import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.channel.*;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import java.net.InetSocketAddress;

@Slf4j
public class MySQLFrontendHandshakeHandler extends ChannelDuplexHandler implements Passthroughable {

    private static ProxyTable proxyTable = Application.appCtx.getBean(ProxyTable.class);

    private static Bootstrap bootstrap = Application.appCtx.getBean(MySQLBackendProxy.class).getBootstrap();

    private volatile boolean handshakeCompleted = false;

    @Override
    public Channel getPairedChannel(Channel currentChannel) {
        return ProxySessionKeeper.INSTANCE.getSessionByFrontChannelId(currentChannel.id()).getBackendChannel();
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) {
        Channel frontend = ctx.channel();
        Channel backend = connectBackend(frontend);
        ProxySessionKeeper.INSTANCE.register(frontend, backend);
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object frame) {
        if (!handshakeCompleted) {
            passthrough(ctx.channel(), (ByteBuf) frame);
            return;
        }
        ctx.fireChannelRead(frame);
    }

    @Override
    public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) {
        // todo if handshake success, change handshakeCompleted true
        handshakeCompleted = true;
        ctx.writeAndFlush(msg);
    }

    @SneakyThrows
    private Channel connectBackend(Channel frontendChannel) {
        InetSocketAddress localAddr = (InetSocketAddress) frontendChannel.localAddress();

        ProxyTable.Item item = proxyTable.findByLocal("0.0.0.0", localAddr.getPort())
                .orElseThrow(() -> new RuntimeException("no proxy table item"));

        ChannelFuture channelFuture = bootstrap.connect(item.getRemoteIp(), item.getRemotePort()).sync();
        if (!channelFuture.isSuccess()) {
            throw new RuntimeException("Cannot connect to MySQL server", channelFuture.cause());
        }
        return channelFuture.channel();
    }
}
