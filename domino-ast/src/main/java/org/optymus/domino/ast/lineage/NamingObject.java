package org.optymus.domino.ast.lineage;

import org.optymus.domino.ast.antlr.SqlSegment;
import org.optymus.domino.ast.mysql.Lineage;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * @author outma
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class NamingObject extends SqlSegment {
    private String alias;
    private List<Lineage> lineages = new ArrayList<>();

    public NamingObject(String expr, String alias, int startIndex, int endIndex) {
        this.alias = alias;
        this.setExpression(expr);
        this.setStartIndex(startIndex);
        this.setEndIndex(endIndex);
    }
}
