package org.optymus.domino.application.provider;

import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class EventLoopGroupProvider {

    @Bean
    public EventLoopGroup frontendBossGroup() {
        return new NioEventLoopGroup(1);
    }

    @Bean
    public EventLoopGroup frontendWorkerGroup() {
        return new NioEventLoopGroup(2);
    }

    @Bean
    public EventLoopGroup backendWorkerGroup() {
        return new NioEventLoopGroup(2);
    }
}
