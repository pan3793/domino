package org.optymus.domino.ast.catalog;

import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import org.optymus.domino.common.util.ExceptionUtil;
import org.optymus.domino.common.util.JdbcUtil;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

public class MySQLCatalog implements Catalog {

    private DataSource dataSource;

    @Getter
    @Setter
    private String currentDatabase;

    public MySQLCatalog(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    @SneakyThrows
    public List<String> listDatabases() {
        return query("show databases",
                null,
                rs -> ExceptionUtil.convertToRuntimeEx(() -> {
                    List<String> result = new ArrayList<>();
                    while (rs.next())
                        result.add(rs.getString(1));
                    return result;
                }));
    }

    @Override
    @SneakyThrows
    public boolean databaseExist(String database) {
        return query("show databases like ?",
                stmt -> ExceptionUtil.convertToRuntimeEx(() -> stmt.setString(1, database)),
                rs -> ExceptionUtil.convertToRuntimeEx(rs::next));
    }

    @Override
    @SneakyThrows
    public List<String> listTables(String database) {
        return query(String.format("show tables in `%s`", database),
                null,
                rs -> ExceptionUtil.convertToRuntimeEx(() -> {
                    List<String> result = new ArrayList<>();
                    while (rs.next())
                        result.add(rs.getString(1));
                    return result;
                }));
    }

    @Override
    @SneakyThrows
    public boolean tableExist(String database, String table) {
        return query(String.format("show tables in `%s` like ?", database),
                stmt -> ExceptionUtil.convertToRuntimeEx(() -> stmt.setString(1, table)),
                rs -> ExceptionUtil.convertToRuntimeEx(rs::next));
    }

    @Override
    @SneakyThrows
    public Table getTable(String database, String table) {
        return query("select `TABLE_SCHEMA`,`TABLE_NAME`,`COLUMN_NAME`,`ORDINAL_POSITION`," +
                        "`COLUMN_DEFAULT`,`IS_NULLABLE`,`COLUMN_TYPE`,`COLUMN_COMMENT`" +
                        " from information_schema.columns" +
                        " where table_schema=? and table_name=?",
                stmt -> ExceptionUtil.convertToRuntimeEx(() -> {
                    stmt.setString(1, database);
                    stmt.setString(2, table);
                }),
                rs -> ExceptionUtil.convertToRuntimeEx(() -> {
                    List<Field> fields = new ArrayList<>();
                    while (rs.next())
                        fields.add(new Field(
                                rs.getInt("ORDINAL_POSITION"),
                                rs.getString("TABLE_NAME"),
                                rs.getString("COLUMN_TYPE"),
                                "YES".equals(rs.getString("IS_NULLABLE")),
                                rs.getString("COLUMN_DEFAULT"),
                                rs.getString("COLUMN_COMMENT")));
                    return new Table(database, table, fields);
                }));
    }

    @SneakyThrows
    private <R> R query(String sql,
                        @Nullable Consumer<PreparedStatement> parameterSetter,
                        Function<ResultSet, R> resultSetExtractor) {
        PreparedStatement stmt = null;
        ResultSet rs = null;
        try (Connection conn = dataSource.getConnection()) {
            stmt = conn.prepareStatement(sql);
            if (parameterSetter != null)
                parameterSetter.accept(stmt);
            rs = stmt.executeQuery();
            return resultSetExtractor.apply(rs);
        } finally {
            JdbcUtil.closeResultSet(rs);
            JdbcUtil.closeStatement(stmt);
        }
    }
}
