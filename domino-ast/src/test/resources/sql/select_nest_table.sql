select sum(c.amount) money, c.customer_id, concat(c.number, customer_id) blah1, concat(c.customer_id)
from (
         select id,
                number,
                customer_id,
                amount
         from biz_contract
         where amount > 1000
     ) c
;