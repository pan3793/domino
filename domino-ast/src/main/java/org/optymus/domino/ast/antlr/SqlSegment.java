package org.optymus.domino.ast.antlr;

import lombok.Data;

/**
 * @author outma
 */
@Data
public abstract class SqlSegment {
    private String expression;
    private Integer startIndex;
    private Integer endIndex;
}
