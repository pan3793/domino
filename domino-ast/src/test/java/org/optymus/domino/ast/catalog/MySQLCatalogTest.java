package org.optymus.domino.ast.catalog;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import lombok.extern.slf4j.Slf4j;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;
import org.testcontainers.containers.MySQLContainer;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@Slf4j
public class MySQLCatalogTest {

    @ClassRule
    public static MySQLContainer mysql = (MySQLContainer) new MySQLContainer()
            .withInitScript("ddl/ddl_init.sql");

    private static MySQLCatalog catalog;

    @BeforeClass
    public static void init() {
        HikariConfig conf = new HikariConfig();
        conf.setDriverClassName(mysql.getDriverClassName());
        conf.setUsername(mysql.getUsername());
        conf.setPassword(mysql.getPassword());
        conf.setJdbcUrl(mysql.getJdbcUrl());
        catalog = new MySQLCatalog(new HikariDataSource(conf));
        catalog.setCurrentDatabase("test");
    }

    @Test
    public void testListDatabases() {
        log.info(String.join(",", catalog.listDatabases()));
    }

    @Test
    public void testDatabaseExist() {
        assertTrue(catalog.databaseExist("information_schema"));
        assertFalse(catalog.databaseExist("database_not_exist"));
    }

    @Test
    public void testListTables() {
        log.info(String.join(",", catalog.listTables()));
        log.info(String.join(",", catalog.listTables("information_schema")));
    }

    @Test
    public void testTableExist() {
        assertTrue(catalog.tableExist("information_schema", "CHARACTER_SETS"));
        assertFalse(catalog.tableExist("table_not_exist"));
    }

    @Test
    public void testGetTable() {
        catalog.listTables("test").forEach(tableName -> {
            Table table = catalog.getTable("test", tableName);
            log.info(table.toString());
        });
    }
}