package org.optymus.domino.ast.catalog;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class Table {
    private String database;
    private String name;
    private List<Field> fields;
}
