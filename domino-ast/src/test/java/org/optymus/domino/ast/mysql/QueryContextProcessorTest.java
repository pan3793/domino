package org.optymus.domino.ast.mysql;

import org.junit.Ignore;
import org.optymus.domino.ast.antlr.CaseChangingCharStream;
import org.optymus.domino.ast.antlr.MySqlLexer;
import org.optymus.domino.ast.antlr.MySqlParser;
import org.optymus.domino.ast.antlr.ParseErrorListener;
import org.optymus.domino.ast.lineage.LineageDAGDepthWalker;
import org.optymus.domino.ast.lineage.NamingObject;
import lombok.SneakyThrows;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.junit.Assert;
import org.junit.Test;

import java.io.InputStream;
import java.util.*;

/**
 * @author outma
 */
public class QueryContextProcessorTest {
    @Test
    public void testSingleTableWithDefaultSchema() {
        // select id,
        //       number,
        //       customer_id,
        //       amount
        // from biz_contract
        // where amount > 1000
        List<NamingObject> results = initAndWalk("sql/select_single_table_with_default_schema.sql");

        Map<String, List<Lineage>> expectPairMap = new HashMap<>();
        expectPairMap.put("id", Collections.singletonList(new Lineage("id", "biz_contract.id")));
        expectPairMap.put("number", Collections.singletonList(new Lineage("number", "biz_contract.number")));
        expectPairMap.put("customer_id", Collections.singletonList(new Lineage("customer_id", "biz_contract.customer_id")));
        expectPairMap.put("amount", Collections.singletonList(new Lineage("amount", "biz_contract.amount")));

        assertAllMatch(expectPairMap, results);
    }

    @Test
    public void testSingleTable() {
        // select c.id,
        //       number as no,
        //       amount    price
        // from dp.contract as c
        // where amount > 100
        // order by id
        // limit 2;
        List<NamingObject> results = initAndWalk("sql/select_single_table.sql");

        Map<String, List<Lineage>> expectPairMap = new HashMap<>();
        expectPairMap.put("c.id", Collections.singletonList(new Lineage("id", "biz_contract.id")));
        expectPairMap.put("no", Collections.singletonList(new Lineage("number", "biz_contract.number")));
        expectPairMap.put("price", Collections.singletonList(new Lineage("amount", "biz_contract.amount")));

        assertAllMatch(expectPairMap, results);
    }

    @Test
    public void testNestedTable() {
        // select sum(c.amount) money, c.customer_id, concat(c.number, customer_id) blah1, concat(c.customer_id)
        // from (
        //         select id,
        //                number,
        //                customer_id,
        //                amount
        //         from biz_contract
        //         where amount > 1000
        //     ) c
        //;
        List<NamingObject> results = initAndWalk("sql/select_nest_table.sql");

        Map<String, List<Lineage>> expectPairMap = new HashMap<>();
        expectPairMap.put("money", Collections.singletonList(new Lineage("amount", "biz_contract.amount")));
        expectPairMap.put("c.customer_id", Collections.singletonList(new Lineage("customer_id", "biz_contract.customer_id")));
        expectPairMap.put("blah1", Arrays.asList(new Lineage("customer_id", "biz_contract.customer_id"), new Lineage("number", "biz_contract.number")));
        expectPairMap.put("concat(c.customer_id)", Collections.singletonList(new Lineage("customer_id", "biz_contract.customer_id")));

        assertAllMatch(expectPairMap, results);
    }

    @Test
    @Ignore
    public void testNested3Table() {
        // select * from ( select id from ( select id from t) a ) b;
        List<NamingObject> results = initAndWalk("sql/select_nest_3_table.sql");

        Map<String, List<Lineage>> expectPairMap = new HashMap<>();
        expectPairMap.put("id", Collections.singletonList(new Lineage("id", "t.id")));

        assertAllMatch(expectPairMap, results);
    }

    @Test
    public void testJoinedTable() {
        // select b1.id,
        //       b1.number,
        //       b1.customer_id,
        //       b2.amount
        // from biz_contract as b1
        //         join biz_plan as b2 on b1.contract_id = b2.contract_id
        List<NamingObject> results = initAndWalk("sql/select_join_table.sql");

        Map<String, List<Lineage>> expectPairMap = new HashMap<>();
        expectPairMap.put("b1.id", Collections.singletonList(new Lineage("id", "biz_contract.id")));
        expectPairMap.put("b1.number", Collections.singletonList(new Lineage("number", "biz_contract.number")));
        expectPairMap.put("b1.customer_id", Collections.singletonList(new Lineage("customer_id", "biz_contract.customer_id")));
        expectPairMap.put("b2.amount", Collections.singletonList(new Lineage("amount", "biz_plan.amount")));
        expectPairMap.put("plan_id", Collections.singletonList(new Lineage("plan_id", "biz_plan.plan_id")));

        assertAllMatch(expectPairMap, results);
    }

    @Test
    public void testSingleTableWithAggregateFunc() {
        // select sum(distinct amount) all_amount,
        //       'literal' as         addition_col
        // from dp.contract
        // group by customer_id
        // having all_amount > 1000;
        List<NamingObject> results = initAndWalk("sql/select_single_table_with_aggregate.sql");

        Map<String, List<Lineage>> expectPairMap = new HashMap<>();
        expectPairMap.put("all_amount", Collections.singletonList(new Lineage("amount", "dp.contract.amount")));

        assertAllMatch(expectPairMap, results);
    }

    @Test
    public void testComplex() {
        // select distinct t1.name,
        //                plan_id,
        //                'literal' as addition_col
        // from dp.customer t1
        //         join
        //     (
        //         select c1.id                     cid,
        //                plan_id,
        //                rp.actual_amount + 100 as pay_amount,
        //                case
        //                    when rp.status = 0
        //                        then '零'
        //                    else
        //                        rp.status
        //                    end                   rp_state
        //         from dp.customer c1
        //                  join dp.contract c2 on c1.id = c2.customer_id
        //                  left join dp.biz_plan as rp
        //                            on c2.id = rp.contract_id
        //                                and rp.create_time > '2018-02'
        //     ) t2
        //     on t1.id = t2.cid
        // limit 3;
        List<NamingObject> results = initAndWalk("sql/select_complex.sql");

        Map<String, List<Lineage>> expectPairMap = new HashMap<>();
        expectPairMap.put("t1.name", Collections.singletonList(new Lineage("amount", "dp.customer.name")));
        expectPairMap.put("plan_id", Collections.singletonList(new Lineage("amount", "dp.biz_plan.plan_id")));

        assertAllMatch(expectPairMap, results);
    }

    @Test
    public void testSingleTableWithStar() {
        // select *, 'literal' as 'dummy'
        // from dp.contract;
        List<NamingObject> results = initAndWalk("sql/select_single_table_with_star.sql");

        Map<String, List<Lineage>> expectPairMap = new HashMap<>();
        expectPairMap.put("*", Arrays.asList(
                new Lineage("id", "dp.contract.id"),
                new Lineage("number", "dp.contract.number"),
                new Lineage("customer_id", "dp.contract.customer_id"),
                new Lineage("amount", "dp.contract.amount")
        ));

        assertAllMatch(expectPairMap, results);
    }

    @Test
    public void testSingleTableWithAliasedStar() {
        // select a.*, 'literal' as 'dummy'
        // from dp.contract a;
        List<NamingObject> results = initAndWalk("sql/select_single_table_with_aliased_star.sql");

        Map<String, List<Lineage>> expectPairMap = new HashMap<>();
        expectPairMap.put("a.*", Arrays.asList(
                new Lineage("a.id", "dp.contract.id"),
                new Lineage("a.number", "dp.contract.number"),
                new Lineage("a.customer_id", "dp.contract.customer_id"),
                new Lineage("a.amount", "dp.contract.amount")
        ));

        assertAllMatch(expectPairMap, results);
    }

    @Test
    public void testNestedTableWithStar() {
        // select *
        // from (
        //         select *, 'literal' as 'dummy'
        //         from dp.contract
        //     );
        List<NamingObject> results = initAndWalk("sql/select_aliased_nest_table_with_star.sql");

        Map<String, List<Lineage>> expectPairMap = new HashMap<>();
        expectPairMap.put("a.*", Arrays.asList(
                new Lineage("a.id", "dp.contract.id"),
                new Lineage("a.number", "dp.contract.number"),
                new Lineage("a.customer_id", "dp.contract.customer_id"),
                new Lineage("a.amount", "dp.contract.amount")
        ));

        assertAllMatch(expectPairMap, results);
    }

    @Test
    public void testJoinedTableWithStar() {
        // select *, 'literal' as 'dummy'
        // from
        //    dp.contract dp1 join dp.contract_extra dp2 on dp1.id = dp2.id
        List<NamingObject> results = initAndWalk("sql/select_joined_table_with_star.sql");

        Map<String, List<Lineage>> expectPairMap = new HashMap<>();
        expectPairMap.put("*", Arrays.asList(
                new Lineage("dp1.id", "dp.contract.id"),
                new Lineage("dp1.number", "dp.contract.number"),
                new Lineage("dp1.customer_id", "dp.contract.customer_id"),
                new Lineage("dp1.amount", "dp.contract.amount"),
                new Lineage("dp2.id", "dp.contract_extra.id"),
                new Lineage("dp2.number", "dp.contract_extra.number"),
                new Lineage("dp2.customer_id", "dp.contract_extra.customer_id"),
                new Lineage("dp2.extra", "dp.contract_extra.extra")
        ));

        assertAllMatch(expectPairMap, results);
    }

    @Test
    public void testJoinedNestedTableWithStar() {
        // select *, 'literal' as 'dummy'
        // from (
        //         select *
        //         from dp.contract
        //     ) dp1
        //         join dp.contract_extra dp2 on dp1.id = dp2.id
        List<NamingObject> results = initAndWalk("sql/select_joined_nested_table_with_star.sql");

        Map<String, List<Lineage>> expectPairMap = new HashMap<>();
        expectPairMap.put("*", Arrays.asList(
                new Lineage("dp1.id", "dp.contract.id"),
                new Lineage("dp1.number", "dp.contract.number"),
                new Lineage("dp1.customer_id", "dp.contract.customer_id"),
                new Lineage("dp1.amount", "dp.contract.amount"),
                new Lineage("dp2.id", "dp.contract_extra.id"),
                new Lineage("dp2.number", "dp.contract_extra.number"),
                new Lineage("dp2.customer_id", "dp.contract_extra.customer_id"),
                new Lineage("dp2.extra", "dp.contract_extra.extra")
        ));

        assertAllMatch(expectPairMap, results);
    }

    @Test
    public void testJoinedAllNestedTableWithStar() {
        // select *, 'literal' as 'dummy'
        // from (
        //         select *
        //         from dp.contract
        //     ) dp1
        //         join (
        //    select *
        //    from dp.contract_extra
        // ) dp2 on dp1.id = dp2.id
        List<NamingObject> results = initAndWalk("sql/select_joined_all_nested_table_with_star.sql");

        Map<String, List<Lineage>> expectPairMap = new HashMap<>();
        expectPairMap.put("*", Arrays.asList(
                new Lineage("dp1.id", "dp.contract.id"),
                new Lineage("dp1.number", "dp.contract.number"),
                new Lineage("dp1.customer_id", "dp.contract.customer_id"),
                new Lineage("dp1.amount", "dp.contract.amount"),
                new Lineage("dp2.id", "dp.contract_extra.id"),
                new Lineage("dp2.number", "dp.contract_extra.number"),
                new Lineage("dp2.customer_id", "dp.contract_extra.customer_id"),
                new Lineage("dp2.extra", "dp.contract_extra.extra")
        ));

        assertAllMatch(expectPairMap, results);
    }

    @Test
    public void testJoinedTableWithStarComplex() {
        // select dp2.*, plan_id, 'literal' as 'dummy'
        // from (
        //         select *
        //         from dp.contract
        //     ) dp1
        //         join (
        //    select *
        //    from dp.contract_extra
        // ) dp2 on dp1.id = dp2.id
        //         join dp.biz_plan dp3 on dp3.plan_id = dp2.id
        List<NamingObject> results = initAndWalk("sql/select_joined_table_with_star_complex.sql");

        Map<String, List<Lineage>> expectPairMap = new HashMap<>();
        expectPairMap.put("dp2.*", Arrays.asList(
                new Lineage("dp2.id", "dp.contract_extra.id"),
                new Lineage("dp2.number", "dp.contract_extra.number"),
                new Lineage("dp2.customer_id", "dp.contract_extra.customer_id"),
                new Lineage("dp2.extra", "dp.contract_extra.extra")
        ));
        expectPairMap.put("plan_id", Collections.singletonList(new Lineage("plan_id", "dp.biz_plan.plan_id")));

        assertAllMatch(expectPairMap, results);
    }

    @SneakyThrows
    private List<NamingObject> initAndWalk(String sqlFileName) {
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream(sqlFileName);
        Assert.assertNotNull("sql file " + sqlFileName + " not exist!", inputStream);

        CharStream charStream = CharStreams.fromStream(inputStream);
        MySqlLexer lexer = new MySqlLexer(new CaseChangingCharStream(charStream, true));
        MySqlParser parser = new MySqlParser(new CommonTokenStream(lexer));
        QueryParserListener queryParserListener = new QueryParserListener();

        parser.removeErrorListeners();
        parser.addErrorListener(new ParseErrorListener());

        ParseTreeWalker.DEFAULT.walk(queryParserListener, parser.root());

        LineageDAGDepthWalker<QueryContext> walker = new LineageDAGDepthWalker<>(queryParserListener.getLineageDAG(), new QueryContextProcessor());
        walker.walk();

        return queryParserListener.getLineageDAG().root().value().selectElements();
    }

    private void assertPartialMatch(Map<String, List<Lineage>> expected, List<NamingObject> actual) {
        expected.forEach((col, expectedLineages) -> {
            NamingObject matchedNamingObject = actual.stream()
                    .filter(lineage -> lineage.getAlias().equals(col))
                    .findFirst().orElseThrow(() -> new RuntimeException("col[" + col + "] not exist!"));
            List<Lineage> matchedLineages = matchedNamingObject.getLineages();

            assertLineageListEquals(expectedLineages, matchedLineages);
        });
    }

    private void assertAllMatch(Map<String, List<Lineage>> expected, List<NamingObject> actual) {
        Assert.assertEquals(expected.size(), actual.size());
        assertPartialMatch(expected, actual);
    }

    private void assertLineageListEquals(List<Lineage> expected, List<Lineage> actual) {
        Assert.assertEquals(expected.size(), actual.size());

        expected.sort(Comparator.comparing(Lineage::getAncestor));
        actual.sort(Comparator.comparing(Lineage::getAncestor));
        for (int i = 0; i < expected.size(); i++) {
            Assert.assertEquals(expected.get(i).getAncestor(), actual.get(i).getAncestor());
        }
    }
}
