package org.optymus.domino.application.conf;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import lombok.Data;
import lombok.Getter;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.antlr.v4.runtime.misc.Pair;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.*;

@Slf4j
@Component
public class ProxyTable implements ApplicationContextAware {
    public static final String SEP;
    public static final String USER_HOME;
    public static List<String> CONF_LOCATIONS;

    static {
        SEP = File.separator;
        USER_HOME = System.getProperty("user.home");
        CONF_LOCATIONS = Arrays.asList(
                USER_HOME + SEP + ".domino" + SEP + "proxy-table.json",
                "/etc/domino/proxy-table.json",
                "classpath:proxy-table.json"
        );
    }

    @Getter
    private List<Item> items;

    private Map<String, Item> nameIdx;

    private Map<Pair<String, Integer>, Item> localIdx;

    private Map<Pair<String, Integer>, Item> remoteIdx;

    private ApplicationContext appCtx;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.appCtx = applicationContext;
    }

    @SneakyThrows
    @PostConstruct
    public void load() {
        InputStream in = null;
        for (String confLocation : CONF_LOCATIONS) {
            if (confLocation.startsWith("classpath:")) {
                Resource resource = appCtx.getResource(confLocation);
                if (resource.exists()) {
                    in = resource.getInputStream();
                }
            } else {
                File file = new File(confLocation);
                if (file.exists()) {
                    in = new FileInputStream(file);
                }
            }
            if (in == null) {
                log.debug("Failed to load configuration from {}", confLocation);
            } else {
                log.info("Load configuration from {} successful.", confLocation);
                break;
            }
        }

        if (in == null) {
            StringJoiner joiner = new StringJoiner(", ", "[", "]");
            for (String confLocation : CONF_LOCATIONS) {
                joiner.add(confLocation);
            }
            throw new RuntimeException("Could not found configuration from locations: " + joiner.toString());
        }

        this.items = new ObjectMapper().readValue(in, new TypeReference<List<Item>>() {
        });
        buildNameIdx();
        buildLocalIdx();
        buildRemoteIdx();
    }

    private void buildNameIdx() {
        Map<String, Item> idx = new HashMap<>();
        items.forEach(item -> idx.put(item.name, item));
        this.nameIdx = idx;
    }

    private void buildLocalIdx() {
        Map<Pair<String, Integer>, Item> idx = new HashMap<>();
        items.forEach(item -> idx.put(new Pair<>(item.localIp, item.localPort), item));
        this.localIdx = idx;
    }

    private void buildRemoteIdx() {
        Map<Pair<String, Integer>, Item> idx = new HashMap<>();
        items.forEach(item -> idx.put(new Pair<>(item.remoteIp, item.remotePort), item));
        this.remoteIdx = idx;
    }

    public Optional<Item> findByName(String name) {
        return Optional.ofNullable(nameIdx.get(name));
    }

    public Optional<Item> findByLocal(String localIp, Integer localPort) {
        return Optional.ofNullable(localIdx.get(new Pair<>(localIp, localPort)));
    }

    public Optional<Item> findByRemote(String remoteIp, Integer remotePort) {
        return Optional.ofNullable(remoteIdx.get(new Pair<>(remoteIp, remotePort)));
    }

    @Data
    @JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
    public static class Item {
        private String name;
        private String localIp;
        private Integer localPort;
        private String remoteIp;
        private Integer remotePort;
    }
}
