package org.optymus.domino.ast.lineage;

import org.jetbrains.annotations.NotNull;

/**
 * @author outma
 */
public class LineageDAGDepthWalker<T extends LineageContext> {
    private DAG<T> graph;
    private LineageProcessor<T> processor;

    public LineageDAGDepthWalker(@NotNull DAG<T> graph, @NotNull LineageProcessor<T> processor) {
        this.graph = graph;
        this.processor = processor;
    }

    public synchronized void walk() {
        walk(graph.root());
    }

    private void walk(DAGVertex<T> vertex) {
        if (vertex.children().isEmpty()) {
            this.processor.process(vertex);
        } else {
            for (DAGVertex<T> child : vertex.children()) {
                walk(child);
            }
            // process itself
            this.processor.process(vertex);
        }
    }
}
