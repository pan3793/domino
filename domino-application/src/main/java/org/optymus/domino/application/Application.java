package org.optymus.domino.application;

import org.optymus.domino.application.conf.ProxyTable;
import org.optymus.domino.application.netty.MySQLFrontendProxy;
import io.netty.bootstrap.ServerBootstrap;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

@Slf4j
public class Application {

    public static AnnotationConfigApplicationContext appCtx;

    @SneakyThrows
    public static void main(String[] args) {
        Application.appCtx = new AnnotationConfigApplicationContext("org.optymus.domino.application");
        appCtx.registerShutdownHook();

        ProxyTable proxyTable = appCtx.getBean(ProxyTable.class);
        MySQLFrontendProxy frontendProxy = appCtx.getBean(MySQLFrontendProxy.class);
        ServerBootstrap serverBootstrap = frontendProxy.getServerBootstrap();

        for (ProxyTable.Item item : proxyTable.getItems()) {
            log.info("Creating proxy [{}], [{}:{}]->[{}:{}]",
                    item.getName(), item.getLocalIp(), item.getLocalPort(), item.getRemoteIp(), item.getRemotePort());
            serverBootstrap.bind(item.getLocalIp(), item.getLocalPort()).addListener(future -> {
                if (future.isSuccess()) {
                    log.info("Success bind {}:{}", item.getLocalIp(), item.getLocalPort());
                } else {
                    log.error("Failure bind {}:{}", item.getLocalIp(), item.getLocalPort(), future.cause());
                }
            });
        }
    }
}
